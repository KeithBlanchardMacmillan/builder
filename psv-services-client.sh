#!/usr/bin/env zsh
source ~/.zshrc

cd ../psv-services

nvm use 10;
yarn client-start
