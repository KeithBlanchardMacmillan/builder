#!/usr/bin/env zsh
source ~/.zshrc
source consul.sh

source ./db/gradebook-dev-ml.sh
export PGDATABASE=gradebook-iam

export ALLOW_LEGACY_METADATA=true
export USER_METADATA_KEY=https://macmillantech.com/user_metadata
export APP_METADATA_KEY=https://macmillantech.com/app_metadata

# Start development server
cd ../ml-iam
nvm use 10;
yarn dev
