#!/usr/bin/env zsh
source ~/.zshrc

source ./db/gradebook-dev-ml.sh
export PGDATABASE=gradebook-cw

export export IAM_URL=http://iam-local.mldev.cloud:5002

# Values copied from console dev/gradebook
export PLATFORM_ADS_URL=https://dev-gradebook-ads.mldev.cloud/v1/ads
export PLATFORM_LAUNCH_BASE_URL=https://dev-gradebook-plat.mldev.cloud
export PLATFORM_SERVICES_URL_PREFIX=https://dev-gradebook-plat.
export PLATFORM_SERVICES_PORT=5001

export WRITE_KEY=35c109a3e9c70b6695d41abc2c90f5b4
export WRITING_CREATE_BASE_URL=https://dev-gradebook-writing.mldev.cloud
export WRITING_LAUNCH_BASE_URL=https://dev-gradebook-writing.mldev.cloud
export WRITING_LAUNCH_PATH_URL="/"
export WRITING_LAUNCH_PREFIX_URL=https://dev-gradebook-writing.

export SF_ENV=true
export LCRP_CURVE_LAUNCH_PREFIX_URL=http://dev-achieve-learningcurve.
export ENABLED_ACCESS_TOKEN_COOKIE=true
export GRADEBOOK_API_URL_PREFIX=https://dev-gradebook-gradebook-api.
export IS_NEW_GRADEBOOK_SERVICE=true
export GRADEBOOK_ENABLE_FILTERS=true
export ICLICKER_ENABLED=true

# Start development server
cd ../psv-services
nvm use 10;
yarn dev:no-db
