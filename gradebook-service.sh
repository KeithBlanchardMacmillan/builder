#!/usr/bin/env zsh
source ~/.zshrc
source consul.sh

source ./db/gradebook-dev-ml.sh
export PGDATABASE=gradebook-cw

export ENABLED_ACCESS_TOKEN_COOKIE=true

# Start development server
cd ../gradebook-service
nvm use 10
yarn dev

