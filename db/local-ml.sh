#!/usr/bin/env bash
export PGHOST=localhost
export PGPASSWORD=postgres
export PGPORT=5432
export PGUSER=postgres
export PGDATABASE=ml