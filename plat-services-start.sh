#!/usr/bin/env zsh
source ~/.zshrc
source consul.sh
source ./db/gradebook-dev-ml.sh
export PGDATABASE=gradebook-plat

export PLAT_GQL_URL=http://plat-local.mldev.cloud:5001/graphql
export IAM_GQL_URL=http://iam-local.mldev.cloud:5002/gql
export WRITING_GQL_URL=https://dev-gradebook-writing-api.mldev.cloud/graphql
export ASSESS_GQL_URL=https://dev-gradebook-assess.mldev.cloud/graphql
export PGUSER=psv_master
export PGHOST=dev-courseware-team.cuhuctv9amqn.us-east-1.rds.amazonaws.com
export PGPASSWORD=AnKpNvmRZ7aCU9Y
export PGPORT=5432
export CONSUL_PATH=achieve-local/

# Start development server
cd ../plat-services
nvm use 8;
yarn start | ./node_modules/.bin/bunyan
